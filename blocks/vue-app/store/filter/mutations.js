export default {
	updateControls: (state, payload) => {
		state.controls = payload
	},

	updateCount: (state, payload) => {
		state.count = payload
	},

	setFloatButtonTimer: (state, { timer }) => {
		state.floatButton.timer = timer
	},

	clearFloatButtonTimer: (state) => {
		clearTimeout(state.floatButton.timer)
	},

	setShowFloatButton: (state, { show }) => {
		state.floatButton.show = show
	},

	setFloatButtonOffset: (state, { top, left }) => {
		state.floatButton.top = top
		state.floatButton.left = left
	},

	setActiveFilterCount(state) {
		state.activeFilterCount = state.controls.reduce(
			(acc, control) => (checkControl(control) ? acc + 1 : acc),
			0
		)
	},
}

function checkControl(control) {
	if (control.controlType === 'range') {
		return control.values.some((value) => value)
	}

	if (control.controlType === 'checkboxList') {
		return control.values.some((value) => value.checked)
	}
}
