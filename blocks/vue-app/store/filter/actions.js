import { spaceBetween } from '../../components/helpers/spaceBetween'

const compId = 'filter'
const endpoint = window['filter-endpoint'] || ''

export default {
	updateState: ({ commit }, { controls, count }) => {
		commit('updateControls', controls)
		commit('updateCount', count)
		commit('setActiveFilterCount')
	},

	showFloatButton: ({ state, commit }, { el, filter }) => {
		const offset = spaceBetween(filter, el)

		commit('clearFloatButtonTimer')
		commit('setFloatButtonTimer', {
			timer: setTimeout(() => {
				commit('clearFloatButtonTimer')
				commit('setShowFloatButton', { show: false })
			}, 3000),
		})
		commit('setShowFloatButton', { show: true })
		commit('setFloatButtonOffset', offset)
	},
}
