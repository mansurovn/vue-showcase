const controls = state => state.controls
const count = state => state.count
const floatButtonTimer = state => state.floatButton.timer
const showFloatButton = state => state.floatButton.show
const floatButtonTop = state => state.floatButton.top
const floatButtonLeft = state => state.floatButton.left
const activeFilterCount = state => state.activeFilterCount

export default {
  controls,
  count,
  activeFilterCount,
  floatButtonTimer,
  floatButtonTop,
  floatButtonLeft,
  showFloatButton
}

