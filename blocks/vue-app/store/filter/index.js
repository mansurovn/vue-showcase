import getters from './getters'
import mutations from './mutations'
import actions from './actions'

let state = {
	status: 'NOT_LOADING',
	controls: [],
	count: 0,
	activeFilterCount: 0,
	floatButton: {
		show: false,
		timer: 0,
		top: 0,
		left: 0
	}
}

if (window.filterDefaultStore) {
	state = $.extend(true, state, window.filterDefaultStore)
}

export const filter = {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
}
