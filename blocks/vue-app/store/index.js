import Vue from 'vue'
import Vuex from 'vuex'
import { search } from './search'
import { cart } from './cart'
import { favorite } from './favorites'
import { filter } from './filter'
import { catalogMenu } from './menu'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		search,
		cart,
		favorite,
		filter,
		catalogMenu
	},
})

export { store }
