export default {
  setItems(state, {sections, items}) {
    state.items = items
    state.sections = sections
  }
}
