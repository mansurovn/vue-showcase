export default {
  items: state => {
    return state.items
  },
  sections: state => {
    return state.sections
  }
}
