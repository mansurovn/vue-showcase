import querystring from 'querystring'
import cloneDeep from 'lodash/cloneDeep'
import { fetch as fetchPolyfill } from 'whatwg-fetch'

const compId = 'cart'
const endpoint = window['cart-endpoint'] || ''

export default {
  async get ({ dispatch, commit, state }) {
    if (state.status === 'LOADING') {
      return
    }

    await commit('setStatus', 'LOADING')

    let queryString = querystring.stringify({
      action: 'getBasket',
      compid: compId
    })

    let response = await (await fetchPolyfill(
      `${endpoint}?${queryString}`,
      {
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/json;charset=utf-8'
        }
      }
    )).json()

    let data = response.data || null

    dispatch('setData', data)

    await commit('setStatus', 'SUCCESS')
  },

  async add ({ dispatch, commit, state }, { productId, quantity = 1, savePosition = false }) {
    let formData = new FormData
    formData.append('action', 'addItem')
    formData.append('compid', compId)
    formData.append('productId', productId)
    formData.append('quantity', quantity)

    let response = await fetchPolyfill(`${endpoint}`, {
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      body: formData
    })

    response = await response.json()

    dispatch('setData', response.data || null)

    return response
  },

  async set ({ dispatch, commit, state }, { productId, quantity = 1 }) {
    let formData = new FormData()
    formData.append('action', 'setItem')
    formData.append('compid', compId)
    formData.append('productId', productId)
    formData.append('quantity', quantity)

    let response = await fetchPolyfill(`${endpoint}`, {
      method: 'POST',
      body: formData,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })

    await dispatch('setData', response.data || null)

    return response
  },

  async remove ({ dispatch, commit, state }, { productId }) {
    let formData = new FormData()
    formData.append('action', 'removeItem')
    formData.append('compid', compId)
    formData.append('productId', productId);


    await commit('setQuantity', { productId: productId, quantity: 0 });
    await commit('setRemove', { productId: productId });

    let response = await fetchPolyfill(`${endpoint}`, {
      method: 'POST',
      body: formData,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })

    response = await response.json()

    dispatch('setData', response.data || null)

    return response
  },

  async clearCartList ({ dispatch, commit, state }) {
    let formData = new FormData()
    formData.append('action', 'removeAll')
    formData.append('compid', compId)

    await commit('removeAll')

    let response = await fetchPolyfill(`${endpoint}`, {
      method: 'POST',
      body: formData,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })

    dispatch('setData', response.data || null)

    return response
  },

  async setData ({ commit }, data) {
    if (data) {
      if (data.links) {
        await commit('setLinks', data.links)
      }

      if (data.items) {
        await commit('setItems', { items: data.items })

        bLazy.revalidate();
      }

      if (data.totalPrice) {
        await commit('setTotalPrice', data.totalPrice)
      }
    }
  }
}
