export default {
  setTotalPrice (state, totalPrice) {
    state.totalPrice = totalPrice
  },

  setLinks (state, links) {
    state.links = links
  },

  setItems (state, payload) {
    state.items = payload.items
  },

  addItem (state, { productId, quantity = 1 }) {
    state.items.push({
      productId: productId,
      quantity: quantity
    })
  },

  setQuantity (state, { productId, quantity }) {
    for (let i = 0; i < state.items.length; i++) {
      if (productId === state.items[i].productId) {
        state.items[i].quantity = quantity
      }
    }
  },

  setRemove (state, {productId}) {
    state.items = state.items.filter(item => +item.productId !== +productId)
  },

  removeAll: state => {
    state.items = [];
  },

  setStatus (state, status) {
    state.status = status
  }
}
