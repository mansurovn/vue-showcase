import format from 'format-number'

const formattedNumber = format({ integerSeparator: ' ' })

export default {
  items: state => state.items,

  toCartLink: state => state.links.toCart,

  isLoading: state => state.status === 'LOADING',

  totalCount: state => state.items.length,

  totalPriceFormat: state => {
    return formattedNumber(+state.totalPrice.value)
  },

  totalOldPriceFormat: state => {
    return (state.totalPrice.oldValue && state.totalPrice.oldValue !== state.totalPrice.value) ? formattedNumber(state.totalPrice.oldValue) : 0
  },

  inCart: state => productId => {
    return state.items.filter(item => +item.productId === +productId).length > 0
  },

  isRemove: state => productId => {
    let item = state.items.find(item => +item.productId === +productId)

    return item && item.isRemove === true
  },

  quantity: state => productId => {
    let item = state.items.find(item => +item.productId === +productId)

    if (item) {
      return +item.quantity
    }

    return 1
  },

  price: state => productId => {
    let item = state.items.find(item => +item.productId === +productId)

    if (item) {
      return formattedNumber(item.price.value)
    }

    return 0
  },

  oldPrice: state => productId => {
    let item = state.items.find(item => +item.productId === +productId)

    if (item && +item.price.value < +item.price.oldValue) {
      return formattedNumber(item.price.oldValue)
    }

    return false
  },

  getIdByProductId: state => productId => {
    let item = state.items.find(item => +item.productId === +productId)

    if (item) {
      return +item.id
    }

    return null
  }
}
