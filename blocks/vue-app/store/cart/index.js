import getters from './getters'
import mutations from './mutations'
import actions from './actions'

let state = {
  status: 'NOT_LOADING',
  items: [],
  links: {
    toCart: null
  },
  totalPrice: {
    value: 0,
    oldValue: 0
  }
}

export const cart = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
