import 'idempotent-babel-polyfill'
import { SearchInstance } from './components/search/SearchInstance'
import { FavoriteInstance } from './components/favorite/FavoriteInstance'
import { GalleryInstance } from './components/gallery/GalleryInstance'
import { CartInstance } from './components/cart/CartInstance'
import { FilterInstance } from './components/filter/FilterInstance'
import { CatalogFilterToggleInstance } from './components/catalogFilterToggle/CatalogFilterToggleInstance'

document.addEventListener('DOMContentLoaded', () => {
	new SearchInstance()
	new FavoriteInstance()
	new GalleryInstance()
	new CartInstance()
	new FilterInstance()
	new CatalogFilterToggleInstance()
})
