import Vue from 'vue'
import { store } from '../../store'
import Filter from './Filter'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  'ru': require('./lang/ru.json'),
}

const i18n = new VueI18n({
  locale: 'ru',
  messages,
})

export class FilterInstance {
  constructor (params) {
    this.params = Object.assign(
      {
        filterEl: '#filter',
      },
      params
    )

    this.init()
  }

  init () {
    if (document.querySelector(this.params.filterEl)) {
      new Vue({
        i18n,
        el: this.params.filterEl,
        store,
        render(h) {
          return h(Filter)
        }
      })
    }
  }
}
