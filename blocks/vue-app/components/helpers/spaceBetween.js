export const spaceBetween = (container, el) => {
    const containerParams = container.getBoundingClientRect(),
        elParams = el.getBoundingClientRect()

    return {
        top: elParams.top + el.clientHeight/2 - containerParams.top,
        left: elParams.left + el.clientWidth - containerParams.left
    }
}