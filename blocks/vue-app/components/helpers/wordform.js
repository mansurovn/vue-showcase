"use strict"

export class WordForm {
    constructor(form1, form2, form5) {
        this.form1 = form1;
        this.form2 = form2;
        this.form5 = form5;
    }

    get(count) {
        count %= 100;

        let count1 = count % 10;

        if (count > 10 && count < 20) {
            return this.form5;
        }

        if (count1 > 1 && count1 < 5) {
            return this.form2;
        }

        if (count1 === 1) {
            return this.form1;
        }

        return this.form5;
    }
}