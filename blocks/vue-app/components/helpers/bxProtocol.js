'use strict'

const bxSubmitButtonId = 'set_filter'
const bxResetButtonId = 'del_filter'

export default {
	methods: {
		bxChangeInput(name, value) {
			let bxControl = document.getElementById(name)

			if (bxControl && bxControl.value !== value) {
				$(document).trigger('bx-filter:change-start')

				bxControl.value = value

				let event = new Event('keyup')
				bxControl.dispatchEvent(event)
			}
		},
		bxClickControl(name) {
			let bxControl = document.querySelector(`[for="${name.toString()}"]`)

			if (bxControl) {
				$(document).trigger('bx-filter:change-start')

				let popup = bxControl.closest('.bx-filter-select-block')

				if (popup) {
					popup.click()
				}

				bxControl.click()
			}
		},
		bxClickSubmit() {
			let bxSubmitButton = document.getElementById(bxSubmitButtonId)

			bxSubmitButton && bxSubmitButton.click()
		},
		bxClickReset() {
			let bxResetButton = document.getElementById(bxResetButtonId)

			bxResetButton && bxResetButton.click()
		},
	},
}
