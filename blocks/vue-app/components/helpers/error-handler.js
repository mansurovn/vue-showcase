export const errorHandler = {
  methods: {
    errorHandler(response) {
      if (!response.success) {
        document.dispatchEvent(new CustomEvent('modal-error:open', {
          detail: {
            title: response.error.title,
            text: response.error.text
          }
        }))
      }
    }
  }
}