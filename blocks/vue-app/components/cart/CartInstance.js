import Vue from 'vue'
import { store } from '../../store'
import Cart from './Cart'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  'ru': require('./lang/ru.json'),
}

const i18n = new VueI18n({
  locale: 'ru',
  messages,
})

export class CartInstance {
  constructor (params) {
    this.params = Object.assign(
      {
        cartEl: '#cart',
        types: [
          {
            type: 'on_list',
            selector: 'on_list_'
          }
        ]
      },
      params
    )

    this.init()
  }

  init () {
    if (document.querySelector(this.params.cartEl)) {
      new Vue({
        i18n,
        el: this.params.cartEl,
        store,
        render (h) {
          return h(Cart)
        }
      })
    }
  }
}
