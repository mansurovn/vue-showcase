<?php

if (isset($_REQUEST['action'])) {
    $items = [
        [
            'id' => 1,
            'productId' => 1,
            'quantity' => 1,
            'name' => 'Золотой браслет с янтарем',
            'img' => '/local/assets/images/temp/index/catalog/good4.png',
            'maxQuantity' => 10,
            'sectionName' => 'Браслеты',
            'price' => [
                'discount' => true,
                'value' => '990',
                'oldValue' => 11990,
            ],
            'href' => '#'
        ],
        [
            'id' => 2,
            'productId' => 2,
            'quantity' => 2,
            'name' => 'Серебряное кольцо с бриллиантами «Алмазы Якутии»',
            'img' => '/local/assets/images/temp/index/catalog/good11.png',
            'maxQuantity' => 10,
            'sectionName' => 'Кольца',
            'price' => [
                'discount' => false,
                'value' => 19990,
            ],
            'href' => '#'
        ],
        [
            'id' => 4,
            'productId' => 78,
            'quantity' => 99,
            'name' => 'TISSOT Gentleman Swissmatic',
            'img' => '/local/assets/images/temp/index/catalog/good1.png',
            'maxQuantity' => 99,
            'sectionName' => 'Часы',
            'price' => [
                'discount' => false,
                'value' => 63990,
                'oldValue' => 65990,
            ],
            'href' => '#'
        ]
    ];

    switch ($_REQUEST['action']) {
        case 'addItem':
            $items = addToCartList($items);
            break;
        case 'setItem':
            $items = setToCartList($items);
            break;
        case 'removeItem':
            $items = removeFromCartList($items);
            break;
        case 'removeAll':
            response();
            break;
    }

    response($items);
}

function addToCartList($items): array
{
    if (isset($_REQUEST['productId']) && (int)$_REQUEST['productId'] === 87) {
        echo json_encode([
            'success' => false,
            'error' => [
                'title' => 'Ошибка корзина',
                'text' => 'Ошибка добавления товара в корзину Доступное для покупки количество товара "Тестовый товар": 25'
            ]
        ]);
    } else {
        if (isset($_REQUEST['productId'])) {
            $productId = $_REQUEST['productId'];
        } else {
            $productId = 69;
        }

        $items[] = [
            [
                'id' => count($items),
                'productId' => $productId,
                'quantity' => 1,
                'name' => 'Колье из красного золота',
                'img' => '/local/assets/images/temp/index/catalog/good7.png',
                'maxQuantity' => 10,
                'price' => [
                    'discount' => true,
                    'value' => 19990,
                    'oldValue' => 22990,
                ],
                'href' => '#'
            ]
        ];

        return $items;
    }
}

function setToCartList($items): array
{
    if (isset($_REQUEST['productId'])) {
        $productId = $_REQUEST['productId'];
    } else {
        $productId = 69;
    }
    if (isset($_REQUEST['quantity'])) {
        $quantity = $_REQUEST['quantity'];
    } else {
        $quantity = 1;
    }

    return $items[] = [
        'id' => count($items),
        'productId' => $productId,
        'quantity' => $quantity,
        'img' => '/local/assets/images/temp/index/catalog/good7.png',
        'name' => 'Колье из красного золота',
        'maxQuantity' => 10,
        'price' => [
            'discount' => true,
            'value' => 19990,
            'oldValue' => 22990,
        ],
        'href' => '#'
    ];
}

function removeFromCartList($items): array
{
    return [$items[0], $items[1]];
}

function response($items)
{
    sleep(2);

    if (empty($items)) {
        echo json_encode([
            'success' => true,
            'data' => [
                'links' => [
                    'toCart' => '#cart'
                ],
            ]
        ]);
    } else {
        echo json_encode([
            'success' => true,
            'data' => [
                'links' => [
                    'toCart' => '#cart'
                ],
                'items' => $items,
                'totalPrice' => [
                    'value' => 93970,
                ]
            ]
        ]);
    }
}