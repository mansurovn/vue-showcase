<?php

return [
    'store' => \json_encode([
        'count' => 12,
        'controls' => [
            /*[
                'controlType' => 'list',
                'label' => 'Категория',
                'multiple' => true,
                'index' => 1,
                'values' => [
                    [
                        'label' => 'Часы',
                        'disabled' => false,
                        'name' => 'WATCHES',
                        'checked' => false,
                        'index' => 1,
                        'count' => null
                    ],
                    [
                        'label' => 'Серьги',
                        'disabled' => false,
                        'name' => 'EARRING',
                        'checked' => true,
                        'index' => 2,
                        'count' => null
                    ],
                    [
                        'label' => 'Подвески',
                        'disabled' => false,
                        'name' => 'PENDANT',
                        'checked' => false,
                        'index' => 3,
                        'count' => null
                    ],
                    [
                        'label' => 'Броши',
                        'disabled' => false,
                        'name' => 'BROOCHES',
                        'checked' => false,
                        'index' => 5,
                        'count' => null
                    ],
                    [
                        'label' => 'Браслеты',
                        'disabled' => false,
                        'name' => 'BRACELETS',
                        'checked' => false,
                        'index' => 6,
                        'count' => null
                    ],
                    [
                        'label' => 'Серьги',
                        'disabled' => false,
                        'name' => 'EARRING',
                        'checked' => false,
                        'index' => 7,
                        'count' => null
                    ],
                    [
                        'label' => 'Шок цена',
                        'disabled' => false,
                        'name' => 'SALE',
                        'checked' => false,
                        'index' => 8,
                        'count' => null
                    ],
                ],
            ],*/
            [
                'index' => 1,
                'controlType' => 'range',
                'label' => 'Цена',
                'options' => [
                    'withSale' => true,
                    'bxNameForSale' => 'price_sale',
                    'measurement' => '',
                ],
                'values' => [
                    [
                        'type' => 'min',
                        'bxName' => 'price_min',
                        'value' => 1300,
                        'placeholder' => 1300,
                    ],
                    [
                        'type' => 'max',
                        'bxName' => 'price_max',
                        'value' => 192600,
                        'placeholder' => 192600,
                    ]
                ],
            ],
            [
                'index' => 2,
                'controlType' => 'checkboxList',
                'label' => 'Бренды',
                'name' => 'brands[]',
                'options' => [
                    'multiple' => true,
                    'twoCols' => false,
                ],
                'values' => [
                    [
                        'label' => 'Audemars Piguet',
                        'disabled' => false,
                        'value' => 'Audemars Piguet',
                        'bxName' => 'Audemars Piguet',
                        'checked' => true,
                        'index' => 1,
                        'count' => null
                    ],
                    [
                        'label' => 'Breguet',
                        'disabled' => true,
                        'value' => 'Breguet',
                        'bxName' => 'Breguet',
                        'checked' => false,
                        'index' => 2,
                        'count' => null
                    ],
                    [
                        'label' => 'Balman',
                        'disabled' => false,
                        'value' => 'Balman',
                        'bxName' => 'Balman',
                        'checked' => true,
                        'index' => 3,
                        'count' => null
                    ],
                    [
                        'label' => 'Tissot',
                        'disabled' => false,
                        'value' => 'Tissot',
                        'bxName' => 'Tissot',
                        'checked' => false,
                        'index' => 4,
                        'count' => null
                    ],
                    [
                        'label' => 'Omega',
                        'disabled' => false,
                        'value' => 'Omega',
                        'bxName' => 'Omega',
                        'checked' => false,
                        'index' => 5,
                        'count' => null
                    ],
                    [
                        'label' => 'Longines',
                        'disabled' => false,
                        'value' => 'Longines',
                        'bxName' => 'Longines',
                        'checked' => false,
                        'index' => 6,
                        'count' => null
                    ],
                    [
                        'label' => 'Breguet',
                        'disabled' => false,
                        'value' => 'Breguet',
                        'bxName' => 'Breguet',
                        'checked' => false,
                        'index' => 7,
                        'count' => null
                    ],
                    [
                        'label' => 'Tissot',
                        'disabled' => false,
                        'value' => 'Tissot',
                        'bxName' => 'Tissot',
                        'checked' => false,
                        'index' => 8,
                        'count' => null
                    ],
                    [
                        'label' => 'Omega',
                        'disabled' => false,
                        'value' => 'Omega',
                        'bxName' => 'Omega',
                        'checked' => false,
                        'index' => 9,
                        'count' => null
                    ],
                ]
            ],
            [
                'index' => 3,
                'controlType' => 'checkboxList',
                'label' => 'МАТЕРИАЛ',
                'name' => 'material[]',
                'options' => [
                    'multiple' => false,
                    'twoCols' => false,
                ],
                'values' => [
                    [
                        'label' => 'Золото',
                        'disabled' => false,
                        'value' => 'GOLD',
                        'bxName' => 'GOLD',
                        'checked' => false,
                        'index' => 1,
                        'count' => null
                    ],
                    [
                        'label' => 'Серебро',
                        'disabled' => false,
                        'value' => 'SILVER',
                        'bxName' => 'SILVER',
                        'checked' => false,
                        'index' => 2,
                        'count' => null
                    ],
                ],
            ],
            [
                'index' => 4,
                'controlType' => 'checkboxList',
                'label' => 'Проба',
                'name' => 'purity[]',
                'options' => [
                    'multiple' => true,
                    'twoCols' => true,
                ],
                'values' => [
                    [
                        'label' => '925',
                        'disabled' => false,
                        'value' => '925',
                        'bxName' => '925',
                        'checked' => true,
                        'index' => 1,
                        'count' => null
                    ],
                    [
                        'label' => '375',
                        'disabled' => false,
                        'value' => '375',
                        'bxName' => '375',
                        'checked' => false,
                        'index' => 2,
                        'count' => null
                    ],
                    [
                        'label' => '585',
                        'disabled' => false,
                        'value' => '585',
                        'bxName' => '585',
                        'checked' => false,
                        'index' => 3,
                        'count' => null
                    ],
                    [
                        'label' => '750',
                        'disabled' => true,
                        'value' => '750',
                        'bxName' => '750',
                        'checked' => false,
                        'index' => 4,
                        'count' => null
                    ],
                ],
            ],
            [
                'index' => 5,
                'controlType' => 'checkboxList',
                'label' => 'Кому',
                'name' => 'forWhom[]',
                'options' => [
                    'multiple' => false,
                    'twoCols' => false,
                ],
                'values' => [
                    [
                        'label' => 'Мужчинам',
                        'disabled' => false,
                        'value' => 'FOR_MEN',
                        'bxName' => 'FOR_MEN',
                        'checked' => false,
                        'index' => 1,
                        'count' => null
                    ],
                    [
                        'label' => 'Женщинам',
                        'disabled' => false,
                        'value' => 'FOR_WOMEN',
                        'bxName' => 'FOR_WOMEN',
                        'checked' => false,
                        'index' => 2,
                        'count' => null
                    ],
                ],
            ],
        ]
    ])
];
